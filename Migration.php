<?php
include('ReconnectingDb.php');

class Migration
{
    // Credentials for Messaging DB
    const DB_CLIENT_HOST_MESSAGE = 'localhost';
    const DB_CLIENT_USER_MESSAGE = 'flows';
    const DB_CLIENT_PASS_MESSAGE = 'password';

    // Credentials for Flows DB
    const DB_CLIENT_HOST_FLOW = 'localhost';
    const DB_CLIENT_USER_FLOW = 'flows';
    const DB_CLIENT_PASS_FLOW = 'password';

    // Format single client e.g [999068] or clients e.g [999068, 12] or all clients e.g []
    const MIGRATE_CLIENT_IDS = [];

    function __construct()
    {
    }

    public function execute()
    {
        $dbMessage = new \ReconnectingDb(self::DB_CLIENT_HOST_MESSAGE, self::DB_CLIENT_USER_MESSAGE, self::DB_CLIENT_PASS_MESSAGE);
        $dbMessage->setDatabase('messaging');
        $extraCondition = count(self::MIGRATE_CLIENT_IDS) > 0 ? ' and client_id in (' . implode(",", self::MIGRATE_CLIENT_IDS) . ')' : '';
        $inboundRoutes = $dbMessage->query(
            "
            SELECT *
            FROM inbound_routes
            WHERE destination like '%/twiml/applet/sms/%' {$extraCondition}
            ORDER BY client_id, id desc
            ",
            []
        )->fetchAll(\PDO::FETCH_ASSOC);

        $updatedIds = [];
        if (count($inboundRoutes) > 0) {
            $dbFlow = new \ReconnectingDb(self::DB_CLIENT_HOST_FLOW, self::DB_CLIENT_USER_FLOW, self::DB_CLIENT_PASS_FLOW);
            $dbFlow->setDatabase('pcs_ivr');

            foreach ($inboundRoutes as $inboundRoute) {
                // example dst: https://login-1-stg.leaddesk.com/beta/ivr/index.php/999068/twiml/applet/sms/811/start
                $dstSegments = explode('/', $inboundRoute['destination']);
                $dstIdIndex = count($dstSegments) - 2;
                $dstFlowId = $dstSegments[$dstIdIndex];

                if (is_numeric($dstFlowId)) {
                    $resultFlows = $dbFlow->query(
                        "
                        SELECT *
                        FROM flows
                        WHERE orig_id = :orig_id
                        AND type = 'message'
                        ",
                        ['orig_id' => $dstFlowId]
                    )->fetchAll(\PDO::FETCH_ASSOC);

                    if (count($resultFlows) > 0) {
                        if ($dstSegments[$dstIdIndex - 1] !== 'sms') {
                            continue;
                        }
                        $dstSegments[$dstIdIndex - 1] = 'message';
                        $dstSegments[$dstIdIndex] = $resultFlows[0]['id'];
                        $newDst = join('/', $dstSegments);

                        $resultUpdate = $dbMessage->query(
                            "
                            UPDATE inbound_routes
                            SET destination = :destination
                            WHERE id = :id
                            ",
                            [
                                'destination' => $newDst,
                                'id' => $inboundRoute['id']
                            ]
                        );
                        if ($resultUpdate->rowCount() > 0) {
                            $updatedIds[] = $inboundRoute['id'];
                        }
                    }
                }
            }
        }

        $listIds = count($updatedIds) > 0 ? join(', ', $updatedIds) : 'nothing';
        echo ("Updated DB messaging table inbound_routes, updated row ids: {$listIds}");
        echo ("\r\n");
    }
}

$migration = new Migration();
$migration->execute();
