<?php
/**
 * Copied from pbx_queues project
 *
 */
class ReconnectingDb
{
    const MYSQL_BAD_DB_ERROR = 1049;

    private $host = null;
    private $user = null;
    private $password = null;
    private $database = null;
    private $dbh = null;

    /**
     * Creates a new ReconnectingDb object and connects to the database
     *
     * @param string $host Database server domain or IP address
     * @param string $user Username
     * @param string $password Password
     * @param string $database Database
     */
    public function __construct($host, $user, $password, $database = null)
    {
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
        $this->database = $database;

        $this->connect();
    }

    /**
     * Connects to the database
     */
    private function connect()
    {
        $attempts = 3;
        $this->dbh = null;

        $dsn = 'mysql:host=' . $this->host;
        if ($this->database) {
            $dsn .= ';dbname=' . $this->database;
        }
        
        while ($attempts > 0) {
            $attempts--;
            try {
                $this->dbh = new \PDO($dsn, $this->user, $this->password);
                $this->dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                $this->dbh->exec("SET NAMES utf8");
                // \Log::trace("Connected to database at {$this->host}");
                return true;
            } catch (\PDOException $e) {
                if ($e->getCode() == self::MYSQL_BAD_DB_ERROR) {
                    $this->database = null;
                    throw $e;
                }
            }
        }
        throw new \Exception("Database connection was lost");
    }

    /**
     * Runs a database query
     *
     * @param string $sql The query to run
     * @param array $params An associative array containing keys and values for
     * a prepared statement
     *
     * @return \PDOStatement
     */
    public function query($sql, $params = array())
    {
        $attempts = 3;
        
        while ($attempts > 0) {
            $attempts--;
            try {
                if (method_exists($this->dbh, 'prepare')) {
                    $query = $this->dbh->prepare($sql);
                    foreach ($params as $key => $value) {
                        $query->bindValue($key, $value);
                    }
                    $query->execute();
    
                    return $query;
                } else {
                    $e = new \Exception();
                    $this->connect();
                }
            } catch (\PDOException $e) {
                if ($e->getCode() != 'HY000'
                    || !stristr($e->getMessage(), 'server has gone away')) {
                    throw $e;
                }

                $this->connect();
            }
        }
        throw new \Exception("Database connection was lost");
    }

    /**
     * Sets the default database
     *
     * @param string $db Database name
     */
    public function setDatabase($db)
    {
        if ($db != $this->database) {
            $this->database = $db;
            $this->query("USE {$db}");
        }
    }

    /**
     * Returns the last inserted ID
     *
     * @return int
     */
    public function lastInsertId()
    {
        return $this->dbh->lastInsertId();
    }
}
